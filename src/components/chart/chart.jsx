import React from "react";
import {
    G2,
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
    Label,
    Legend,
    View,
    Guide,
    Shape,
    Facet,
    Util
} from "bizcharts";

class Basic extends React.Component {
    render() {
        const data = [
            {
                year: "Sunday",
                value: 3
            },
            {
                year: "Monday",
                value: 4
            },
            {
                year: "Tuesday",
                value: 3.5
            },
            {
                year: "Wednesday",
                value: 5
            },
            {
                year: "Thursday",
                value: 4.9
            }
        ];
        const cols = {
            value: {
                min: 0
            },
            year: {
                range: [0, 1]
            }
        };
        return (
            <div>
                <Chart height={400} data={data} scale={cols} forceFit>
                    <Axis name="year" />
                    <Axis name="value" />
                    <Tooltip
                        crosshairs={{
                            type: "y"
                        }}
                    />
                    <Geom type="line" position="year*value" size={2} />
                    <Geom
                        type="point"
                        position="year*value"
                        size={4}
                        shape={"circle"}
                        style={{
                            stroke: "#fff",
                            lineWidth: 1
                        }}
                    />
                </Chart>
            </div>
        );
    }
}

export default Basic;