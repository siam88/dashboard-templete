import { lazy } from 'react';
import { DASHBOARD_PATH, USER_PATH, PRODUCT_PATH, ADD_PRODUCT_PATH, PRODUCT_INFO_PATH, EDIT_PATH, ORDER_PATH, ORDER_INFO_PATH } from './Slugs';

const Dashboard = lazy(() => import('../components/pages/dashboard/Dashboard'));




const Routes = [
    {
        path: DASHBOARD_PATH,
        exact: true,
        isPrivate: false,
        component: Dashboard
    },

]

export default Routes;