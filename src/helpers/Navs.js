import React from 'react';
import { Icon } from "antd";
import {
    DASHBOARD_PATH, LOGIN_PATH, PAGE_403_PATH,
    PAGE_500_PATH, PAGE_404_PATH, USER_PATH,
    PRODUCT_PATH, ORDER_PATH
} from '../routes/Slugs';

const Navs = [
    {
        key: 'dashboard',
        title: 'Dashboard',
        path: DASHBOARD_PATH,
        icon: <Icon type="pie-chart" />,
        subMenu: null
    }
]

export default Navs;